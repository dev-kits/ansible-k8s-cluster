# Template repo to build a k8s cluster include k8s master, k8s worker, etcd cluster and flunnel service. 

### Tech Stack & Tools
* [ansible](https://www.ansible.com/)

### Prerequisite
#####  Install ansible env
     we prefer to use ansible via docker, please refer to the prerequisite of infra-base
      
##### Update env vars
     Update the inventory file and update group vars.
     
##### Sych up k8s master ssh keys to all nodes.
     
### Include Steps
1. Install k8s binaries
2. Install etcd binaries
3. Install flanneld binaries
4. Create k8s certificate files
5. Install and bring k8s services.



