#!/bin/sh
echo "Deploying $2 to $1"
ENVIRONMENT=$1
ROLE=$2

shift
shift
docker-compose run --rm -w /app ansible ansible-playbook -i environments/${ENVIRONMENT}/inventory ${ROLE}.yml $@
